(function($) {

    $(".js-slideshow").each(function() {
        var slider = $(this),
            getArrowIcon = function(name) {
                return slider.siblings(name).length > 0 ? slider.siblings(name).html() : '';
            },
            arrowPrevIcon = getArrowIcon('.js-slickArrowPrev'),
            arrowNextIcon = getArrowIcon('.js-slickArrowNext'),
            dots = (slider.attr('data-dots') === 'true') ? true : false,
            arrows = (slider.attr('data-arrows') === 'false') ? false : true,
            autoplay = (slider.attr('data-autoplay') === 'true') ? true : false,
            autoplaySpeed = slider.attr('data-autoplaySpeed') ? parseInt(slider.attr('data-autoplaySpeed')) : 5000,
            speed = slider.attr('data-speed') ? parseInt(slider.attr('data-speed')) : 500,
            fade = (slider.attr('data-fade') === 'true') ? true : false,
            cursorNavigation = (slider.attr('data-cursor-navigation') === 'true') ? true : false,
            dataCounter = (slider.attr('data-counter') === 'true') ? true : false;


        // Cursor navigation
        if(cursorNavigation) {
            var getWindowWidth = function() {
                    return (window.innerWidth/2);
                };
            slider.on('click', function(e) {
                if($(e.target).is('a')) return;
                if(e.clientX > getWindowWidth()) {
                    slider.slick('slickNext');
                } else {
                    slider.slick('slickPrev');
                }
            }).on('mouseenter mousemove', function(e) {
                if(e.clientX > getWindowWidth()) {
                    slider.removeClass('prev-cursor').addClass('next-cursor');
                } else {
                    slider.removeClass('next-cursor').addClass('prev-cursor');
                }
            }).on('mouseleave', function(e) {
                slider.removeClass('prev-cursor').removeClass('next-cursor');
            });
        }


        // Counter
        if(dataCounter){
            slider.on('init reInit afterChange', function(event, slick, currentSlide, nextSlide){
                // append counter
                if(!slider.find('.js-slickCounter').length){
                    slider.append('<div class="slick-counter js-slickCounter"></div>');
                }

                // currentSlide is undefined on init
                // set it to 0 in this case (currentSlide is 0 based)
                var i = (currentSlide ? currentSlide : 0) + 1;
                slider.find('.js-slickCounter').text(i + ' / ' + slick.slideCount);
            });
        }

        // Init slider
        slider.slick({
            dots: dots,
            arrows: arrows,
            prevArrow: '<button type="button" class="slick-prev">' + arrowPrevIcon + '</button>',
            nextArrow: '<button type="button" class="slick-next">' + arrowNextIcon + '</button>',
            autoplay: autoplay,
            autoplaySpeed: autoplaySpeed,
            speed: speed,
            fade: fade,
            pauseOnFocus: false,
            pauseOnHover: false,
            //adaptiveHeight: true,
        });

        slider.addClass('loaded');
    });

})(jQuery);