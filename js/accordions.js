(function($) {

    // needed classes: js-accordion, js-toggleAccordion, js-accordionContent

    var animDuration = 500,
        closeAllOpened = true,
        //scrollOffset = $('.js-siteHeader').outerHeight() +1,
        checkOpened = function() {
            $('.js-accordion').each(function() {
                if($(this).hasClass('opened')){
                    $(this).find('.js-accordionContent').show();
                }
            });
        },
        closeOpenedAccordions = function(container) {
            var siblingAccordions = container.siblings('.js-accordion');

            siblingAccordions.each(function() {
                if($(this).hasClass('opened')){
                    $(this).removeClass('opened');
                    $(this).find('.js-accordionContent').slideUp();
                }
            });
        },
        scrollToAccordion = function(container) {
            var target = container.offset().top,
                prevOpened = container.prevAll('.js-accordion.opened');

            // Compensate for previous accordions closing 
            // at the same time this one is opening
            if(prevOpened.length){
                target = target - prevOpened.outerHeight();
            }

            // Scroll to element if not in view
            if(target < $(window).scrollTop()){
                scrollTo(target);
            }

        },
        closeAccordion = function(content, container) {
            // Close content
            container.removeClass('opened');
            content.slideUp(animDuration);
        },
        openAccordion = function(content, container) {
            
            if(closeAllOpened) {
                // Scroll to elem
                scrollToAccordion(container);
                // Close all opened
                closeOpenedAccordions(container);
            }

            // Open content
            container.addClass('opened');
            content.slideDown(animDuration);
        },
        toggleAccordion = function(trigger) {
            var container = trigger.parent('.js-accordion'),
                content = trigger.next('.js-accordionContent');

            if(container.hasClass('opened')){
                closeAccordion(content, container);
            } else {
                openAccordion(content, container);
            }
        };

    // Open those opened on page load
    checkOpened();

    // Event
    $('.js-toggleAccordion').on('click', function(e){
        e.preventDefault();
        toggleAccordion($(this));
    });

})(jQuery);