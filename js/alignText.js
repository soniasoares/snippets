(function($) {

    // Function to vertical align all text to top
    // on items with diferent height images

    // classes needed: js-alignText, js-textToAlign

    var alignTextItems = $('.js-alignText'),
        getLineHeight = function(text) {
            return parseInt(text.css('line-height'));
        },
        countTextLines = function(text) {

            // find how many lines this text has
            // first get line height
            var lineHeight = getLineHeight(text);

            // get text height and divide by line height
            //return text.height() / lineHeight;
            return text.height() / lineHeight;
        },
        getItemPosition = function(item) {
            // to get item position we need to find bottom instead of top
            // this is because items are aligned to flex-end
            // return Math.round(item.position().top + item.outerHeight());

            // changed to top alignament
            return item.position().top;
        }
        getRequiresLines = function(item) {
            // find out how many lines of text are required
            // it needs to find siblings that are on same row current item
            // and get the one with most lines
            var requiredLines = 0;
                itemPosition = getItemPosition(item);

            alignTextItems.each(function() {
                if(getItemPosition($(this)) === itemPosition) {

                    var thisTextLines = countTextLines($(this).find('.js-textToAlign'));

                    if(thisTextLines > requiredLines) {
                        requiredLines = thisTextLines;
                    }
                }
            });

            return requiredLines;
        },
        alignText = function() {
            alignTextItems.each(function() {

                var text = $(this).find('.js-textToAlign');

                // reset extra margin
                $(this).css('margin-bottom', '');

                // find out how many lines of text are required
                var requiredLines = getRequiresLines($(this));

                // find how many lines this text has
                var textLines = countTextLines(text);

                // add missing lines
                if(textLines < requiredLines) {
                    // get current margin
                    var marginBottom = parseInt($(this).css('margin-bottom'));

                    // get line height and add extra margin with missing lines
                    var lineHeight = getLineHeight(text);
                        missingLines = requiredLines - textLines,
                        extraMargin = lineHeight * missingLines;
                    $(this).css('margin-bottom', marginBottom + extraMargin);
                }

            });
        };

    // Init after everything has loaded
    $(window).bind("load", function () {
        alignText();
    });

    // Update after window resize
    $(window).resize(function () { alignText(); });


})(jQuery);