// Section navigation for one page sites,
// needs main.js functions
// js-pageSection with id and js-scrollToSection with href and data-slug

(function($) {

    // SECTION NAVIGATION
    var sectionLinks = $('.js-scrollToSection'),
        pageSections = $('.js-pageSection'),
        canUpdateLinkOnScroll = true,
        getActiveLinks = function() {
            return $('.js-scrollToSection.active');
        },
        isScrollDisabled = function() {
            var html = $('html');
            return (html.hasClass('noscroll') && parseInt(html.css('top')) < 0) ? true : false;
        },
        getOffset = function() {
            return topOffset();
        },
        getLinkBySlug = function(slug) {
            return $('.js-scrollToSection[data-slug="' + slug + '"]');
        }
        deactivateLink = function(link) {
            if(link.length > 0) {
                link.removeClass('active');
            }
        },
        activateLink = function(link) {
            deactivateLink(getActiveLinks().not(link));
            if(link.length > 0) {
                link.addClass('active');
            }
        },
        setHash = function(hash) {
            var currentHash = window.location.hash;
            if(!hash) {
                if(currentHash) {
                    clearHash();
                }
            } else if(currentHash !== hash) {
                updateHash(hash);
            }
        },
        scrollToSection = function(section) {
            canUpdateLinkOnScroll = false;
            var slug = section.attr('id');
            activateLink(getLinkBySlug('#' + slug));
            updateHash('#' + slug);
            scrollTo(section.offset().top - getOffset(), false, 500, function() {
                canUpdateLinkOnScroll = true;
            });
        },
        getActiveSection = function() {
            var activeSection = undefined;
            pageSections.each(function(){
                var thisTop = $(this).offset().top - getOffset() - 1;
                if (thisTop <= $(window).scrollTop() && thisTop + $(this).innerHeight() >= $(window).scrollTop()) {
                    activeSection = $(this);
                }
            });
            return activeSection;
        },
        updateActiveLinkOnScroll = function() {
            if(pageSections.length < 0) return;
            if(!canUpdateLinkOnScroll) return;
            if(isScrollDisabled()) return;

            var activeSection = getActiveSection();
    
            if(activeSection) {
                var slug = activeSection.attr('id'),
                    activeLink = getLinkBySlug('#' + slug);
                
                // update link
                if(!activeLink.hasClass('active')) {
                    activateLink(activeLink);
                }
                
                // update hash var
                if(slug) {
                    setHash('#' + slug);
                } else {
                    setHash(undefined);
                }
            } else {
                deactivateLink(getActiveLinks());
                setHash(undefined);
            }
        };

    // HASH LINKS
    // Check for hash links
    // If there's hash scroll to element by compensating for header
    // remove it if code already in main
    $(document).ready( function() {
        if(window.location.hash) {
            // Only set the timer if you have a hash
            setTimeout(function() {
                scrollToSection($(window.location.hash));
            }, 500);
        }
    });

    sectionLinks.on('click', function(e) {
        e.preventDefault();
        scrollToSection($($(this).attr('href')));
    });

    // Update active link on scroll
    updateActiveLinkOnScroll();
    $(document).scroll(function() {
        updateActiveLinkOnScroll();
    });

})(jQuery);