// CHECK IF ELEMENTS ARE IN VIEW
window.sticky = window.sticky || {};
sticky = {
    elemIsInView: function(elem, scrollOffset) {
        scrollOffset = typeof scrollOffset !== 'undefined' ? scrollOffset : 0;
        var scroll = $(window).scrollTop(),
            screenHeight = window.innerHeight,
            elemTop = elem.offset().top,
            elemHeight = elem.innerHeight(),
            elemBottom = elemTop + elemHeight;
        if(scroll + scrollOffset >= elemTop && scroll + screenHeight <= elemBottom) {
            return 'in-view';
        } else if (scroll + screenHeight > elemBottom && scroll <= elemBottom) {
            return 'leaving-view';
        } else {
            return false;
        }             
    },
    elemIsAtParentBottom: function(elem, parent, scrollOffset) {
        scrollOffset = typeof scrollOffset !== 'undefined' ? scrollOffset : 0;
        var scroll = $(window).scrollTop(),
            elemHeight = elem.innerHeight(),
            parentBottom = parent.offset().top + parent.innerHeight();
        if(scroll + scrollOffset > parentBottom - elemHeight) {
            return true;
        } else {
            return false;
        }
    },
    fixElemInParent: function(elem, parent, scrollOffset) { 
        scrollOffset = typeof scrollOffset !== 'undefined' ? scrollOffset : 0;
        var parentIsInView = this.elemIsInView(parent, scrollOffset) === 'in-view' || this.elemIsInView(parent, scrollOffset) === 'leaving-view' ? true : false; 
        var elemIsAtBottom  = this.elemIsAtParentBottom(elem, parent, scrollOffset);
        if(parentIsInView) {
            if(elemIsAtBottom) {
                elem.removeClass('fixed').addClass('leaving');  
            } else {
                elem.removeClass('leaving').addClass('fixed');
            }
        } else {
            elem.removeClass('fixed').removeClass('leaving');
        }
    },
};